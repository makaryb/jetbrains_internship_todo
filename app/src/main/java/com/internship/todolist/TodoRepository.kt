package com.internship.todolist

import timber.log.Timber
import javax.inject.Inject

class TodoRepository @Inject constructor(
    val localDataSource: TodoLocalDatasource
) {
    val todoItems: List<TodoModel> = localDataSource.todolist

    fun getItem(name: String): TodoModel? {
        return localDataSource.todolist.firstOrNull {
            it.name == name
        }
    }

    fun addItem(item: TodoModel) {
        localDataSource.todolist.plus(item)
    }
}
