package com.internship.todolist.add

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.provider.MediaStore.Images

class PhotoUriManager(private val appContext: Context) {

    private val photoCollection by lazy {
        Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
    }

    private val resolver by lazy { appContext.contentResolver }

    fun buildNewUri() = resolver.insert(photoCollection, buildPhotoDetails())

    private fun buildPhotoDetails() = ContentValues().apply {
        put(Images.Media.DISPLAY_NAME, generateFilename())
        put(Images.Media.MIME_TYPE, "image/jpeg")
    }

    private fun generateFilename() = "myhoa-${System.currentTimeMillis()}.jpg"

    fun getUriUnderQ(): Uri? {
        val values = ContentValues()
        values.put(Images.Media.TITLE, "myhoa-initiative")
        values.put(Images.Media.DISPLAY_NAME, generateFilename())
        values.put(Images.Media.DESCRIPTION, "myhoa-initiative")
        values.put(Images.Media.MIME_TYPE, "image/jpeg")
        values.put(Images.Media.DATE_ADDED, System.currentTimeMillis())

        return resolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values)
    }
}
