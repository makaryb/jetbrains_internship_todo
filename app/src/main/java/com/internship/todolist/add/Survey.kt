package com.internship.todolist.add

import android.net.Uri
import androidx.annotation.StringRes

data class SurveyResult(
    val title: String,
    val description: String,
    val photoUri: Uri?,
)

data class Survey(
    @StringRes val title: Int,
    val questions: List<Question>
)

data class Question(
    val id: Int,
    @StringRes val questionText: Int,
    val answer: PossibleAnswer,
    @StringRes val description: Int? = null
)

enum class SurveyActionType { GET_PHOTO, TAKE_PHOTO }

sealed class SurveyActionResult {
    data class Photo(val uri: Uri?) : SurveyActionResult()
}

sealed class PossibleAnswer {

    data class Action(
        @StringRes val label: Int,
        val actionType: SurveyActionType
    ) : PossibleAnswer()

    data class TitleInfo(
        val defaultValue: String = ""
    ) : PossibleAnswer()

    data class DescriptionInfo(
        val defaultValue: String = ""
    ) : PossibleAnswer()
}

sealed class Answer<T : PossibleAnswer> {

    data class Action(val result: SurveyActionResult) : Answer<PossibleAnswer.Action>()

    data class TitleInfo(val title: String) : Answer<PossibleAnswer.TitleInfo>()

    data class DescriptionInfo(val description: String) : Answer<PossibleAnswer.DescriptionInfo>()
}
