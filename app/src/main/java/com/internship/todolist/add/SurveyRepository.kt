package com.internship.todolist.add

import com.internship.todolist.R
import com.internship.todolist.add.QuestionPeriodicNumber.DESCRIPTION
import com.internship.todolist.add.QuestionPeriodicNumber.PHOTO
import com.internship.todolist.add.QuestionPeriodicNumber.TITLE

// Static data of questions
private val createInitiativeQuestions = mutableListOf(
    Question(
        id = 1,
        questionText = R.string.take_or_select_picture,
        answer = PossibleAnswer.Action(
            label = R.string.add_photo,
            actionType = SurveyActionType.GET_PHOTO
        )
    ),
    Question(
        id = 2,
        questionText = R.string.set_title,
        answer = PossibleAnswer.TitleInfo(),
        description = R.string.set_textfiled
    ),
    Question(
        id = 3,
        questionText = R.string.set_description,
        answer = PossibleAnswer.DescriptionInfo(),
        description = R.string.set_textfiled
    )
).toList()

private val createInitiativeSurvey = Survey(
    title = R.string.which_jetpack_library,
    questions = createInitiativeQuestions
)

object SurveyRepository {

    suspend fun getSurvey() = createInitiativeSurvey

    fun getSurveyResult(answers: List<Answer<*>>): SurveyResult {
        return SurveyResult(
            title = (answers[TITLE] as Answer.TitleInfo).title,
            description = (answers[DESCRIPTION] as Answer.DescriptionInfo).description,
            photoUri = ((answers[PHOTO] as Answer.Action).result as SurveyActionResult.Photo).uri,
        )
    }
}

object QuestionPeriodicNumber {
    const val PHOTO = 0
    const val TITLE = 1
    const val DESCRIPTION = 2
}
