# JetBrains internship ToDo

JetBrains Big Data Tools Internship

ToDo list sample app

Jetpack Compose

*Demonstration:* 

https://disk.yandex.ru/i/PqBmWFR9-CgaPA

*Screenshots:*

<img src="images_for_readme/1.jpg"  width="400" height="720">

<img src="images_for_readme/2.jpg"  width="400" height="720">

<img src="images_for_readme/3.jpg"  width="400" height="720">

<img src="images_for_readme/4.jpg"  width="400" height="720">

<img src="images_for_readme/5.jpg"  width="400" height="720">

<img src="images_for_readme/6.jpg"  width="400" height="720">

<img src="images_for_readme/7.jpg"  width="400" height="720">

<img src="images_for_readme/8.jpg"  width="400" height="720">

<img src="images_for_readme/9.jpg"  width="400" height="720">
