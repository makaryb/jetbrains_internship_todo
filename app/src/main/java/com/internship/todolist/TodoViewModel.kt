package com.internship.todolist

import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TodoViewModel @Inject constructor(
    private val todoRepository: TodoRepository,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : ViewModel() {

    companion object {
        private var INSTANCE: TodoViewModel? = null
        fun getInstance(): TodoViewModel? {
            return INSTANCE
        }
    }

    private val _suggestedTodo = MutableLiveData<List<TodoModel>>()
    val suggestedTodo: LiveData<List<TodoModel>>
        get() = _suggestedTodo

    val hashMap: MutableMap<String, Color>
    init {
        INSTANCE = this
        _suggestedTodo.value = todoRepository.todoItems
        hashMap = mutableStateMapOf("sample" to Color.Transparent)
        for (i in _suggestedTodo.value!!) {
            hashMap[i.name] = Color.Transparent
        }
    }

    var myList = todoRepository.todoItems

    fun addTodo(item: TodoModel) {
        viewModelScope.launch {
            myList = withContext(defaultDispatcher) {
                val newList = myList.toMutableList()
                newList.add(item)
                newList.toList()
            }
            hashMap[item.name] = Color.Transparent
            _suggestedTodo.value = myList
        }
    }

    fun deleteTodo(item: TodoModel) {
        viewModelScope.launch {
            myList = withContext(defaultDispatcher) {
                val newList = myList.toMutableList()
                val id = newList.indexOf(item)
                newList.removeAt(id)
                newList.toList()
            }
            hashMap.remove(item.name)
            _suggestedTodo.value = myList
        }
    }
}
