package com.internship.todolist.add

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import com.internship.todolist.theme.TodolistTheme
import dagger.hilt.android.AndroidEntryPoint

private const val KEY_ARG_RESULT_NAME = "KEY_ARG_RESULT_NAME"
private const val KEY_ARG_RESULT_DESCRIPTION = "KEY_ARG_RESULT_DESCRIPTION"
private const val KEY_ARG_RESULT_URL = "KEY_ARG_RESULT_URL"

@AndroidEntryPoint
class SurveyActivity : ComponentActivity() {

    companion object {
        private var INSTANCE: SurveyActivity? = null
        fun getInstance(): SurveyActivity? {
            return INSTANCE
        }
    }

    private var photoQuestionId: Int? = null

    private val selectImageLauncher = registerForActivityResult(
        ActivityResultContracts.GetContent()
    ) { uri ->
        if (photoQuestionId != null) {
            viewModel.onPhotoPicked(photoQuestionId!!, uri)
        }
    }

    private val takePicture = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { photoSaved ->
        if (photoSaved) {
            if (photoQuestionId != null) {
                viewModel.onImageSaved(photoQuestionId!!)
            }
        }
    }

    val viewModel: SurveyViewModel by viewModels {
        SurveyViewModelFactory(PhotoUriManager(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        INSTANCE = this

        setContent {
            TodolistScaffold {
                viewModel.uiState.observeAsState().value?.let { surveyState ->
                    when (surveyState) {
                        is SurveyState.Questions -> SurveyQuestionsScreen(
                            questions = surveyState,
                            onAction = {
                                id, action ->
                                handleSurveyAction(id, action)
                            },
                            onDonePressed = {
                                viewModel.computeResult(surveyState)
                            },
                            onBackPressed = {
                                this.onBackPressedDispatcher.onBackPressed()
                            }
                        )
                        is SurveyState.Result -> SurveyResultScreen(
                            result = surveyState
                        )
                    }
                }
            }
        }
    }

    private fun handleSurveyAction(
        questionId: Int,
        actionType: SurveyActionType
    ) {
        when (actionType) {
            SurveyActionType.GET_PHOTO -> getAPhoto(questionId = questionId)
            SurveyActionType.TAKE_PHOTO -> takeAPhoto(questionId = questionId)
        }
    }

    private fun getAPhoto(questionId: Int) {
        this.photoQuestionId = questionId
        selectImageLauncher.launch("image/*")
    }

    private fun takeAPhoto(questionId: Int) {
        this.photoQuestionId = questionId

        val imageCaptureFilePath: Uri? = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            viewModel.getUriToSaveImage()
        } else {
            viewModel.getUriToSaveImageUnderQ()
        }

        takePicture.launch(imageCaptureFilePath)
    }

    fun finishAddItemInfoActivity(
        title: String,
        description: String,
        imageUrl: String
    ) {
        val resultIntent = Intent()
        resultIntent.putExtra(KEY_ARG_RESULT_NAME, title)
        resultIntent.putExtra(KEY_ARG_RESULT_DESCRIPTION, description)
        resultIntent.putExtra(KEY_ARG_RESULT_URL, imageUrl)
        setResult(RESULT_OK, resultIntent)
        finish()
    }
}

@Composable
fun TodolistScaffold(content: @Composable () -> Unit) {
    TodolistTheme {
        Surface(color = MaterialTheme.colors.primary) {
            content()
        }
    }
}
