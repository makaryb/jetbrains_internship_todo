package com.internship.todolist

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Tag
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumePositionChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.input.pointer.positionChange
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.internship.todolist.theme.*
import dev.chrisbanes.accompanist.coil.CoilImage
import timber.log.Timber
import kotlin.math.roundToInt

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun TodoScreen(
    onAddItemClicked: () -> Unit
) {
    val viewModel: TodoViewModel = viewModel()
    val suggestedTodos by viewModel.suggestedTodo.observeAsState()

    var setTagVisible by remember { mutableStateOf(false) }

    lateinit var item: TodoModel

    Scaffold(
        content = {
            suggestedTodos?.let { todos ->
                ExploreSection(
                    title = "Where should I go?",
                    exploreList = todos,
                    viewModel = viewModel,
                    onTagClicked = {
                        item = it
                        setTagVisible = true
                    },
                    onRemoveClicked = {
                        viewModel.deleteTodo(it)
                        Timber.i("what:%s", suggestedTodos)
                    },
                    modifier = Modifier.padding(it),
                )
            }

            AnimatedVisibility(
                visible = setTagVisible
            ) {
                Surface(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(36.dp),
                    color = Blue800Light
                ) {
                    Column(
                        modifier = Modifier.padding(24.dp),
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            "What is on your mind?",
                            color = White
                        )
                        Spacer(Modifier.height(52.dp))
                        Row(
                            horizontalArrangement = Arrangement.Center
                        ) {
                            IconButton(
                                modifier = Modifier
                                    .then(Modifier.size(92.dp)),
                                onClick = {
                                    viewModel.hashMap[item.name] = LowGreen
                                    setTagVisible = false
                                }
                            ) {
                                Icon(
                                    painterResource(id = R.drawable.green_square),
                                    "Delete Todo icon button",
                                    tint = Color.Green
                                )
                            }
                            Spacer(Modifier.width(24.dp))
                            Text(
                                "Interested",
                                color = White
                            )
                        }
                        Spacer(Modifier.height(36.dp))
                        Row(
                            horizontalArrangement = Arrangement.Center
                        ) {
                            IconButton(
                                modifier = Modifier
                                    .then(Modifier.size(92.dp)),
                                onClick = {
                                    viewModel.hashMap[item.name] = Color.Transparent
                                    setTagVisible = false
                                }
                            ) {
                                Icon(
                                    painterResource(id = R.drawable.white_square),
                                    "Delete Todo icon button",
                                    tint = Color.White
                                )
                            }
                            Spacer(Modifier.width(24.dp))
                            Text(
                                "Neutral",
                                color = White
                            )
                        }
                        Spacer(Modifier.height(36.dp))
                        Row(
                            horizontalArrangement = Arrangement.Center
                        ) {
                            IconButton(
                                modifier = Modifier
                                    .then(Modifier.size(92.dp)),
                                onClick = {
                                    viewModel.hashMap[item.name] = LowRed
                                    setTagVisible = false
                                }
                            ) {
                                Icon(
                                    painterResource(id = R.drawable.red_square),
                                    "Not intersted button",
                                    tint = Color.Red
                                )
                            }
                            Spacer(Modifier.width(24.dp))
                            Text(
                                "Not interested",
                                color = White
                            )
                        }
                    }
                }
            }
        },
        floatingActionButton = {
            MyFloatingButton(
                onAddItemClicked = onAddItemClicked
            )
        }
    )
}

@Composable
fun MyFloatingButton(
    onAddItemClicked: () -> Unit
) {
    FloatingActionButton(onClick = onAddItemClicked) {
        Icon(imageVector = Icons.Filled.Add, contentDescription = "Add item page")
    }
}

@Composable
fun ExploreSection(
    title: String,
    viewModel: TodoViewModel,
    exploreList: List<TodoModel>,
    onTagClicked: (TodoModel) -> Unit,
    onRemoveClicked: (TodoModel) -> Unit,
    modifier: Modifier,
) {
    Surface(modifier = modifier.fillMaxSize(), color = Color.White, shape = BottomSheetShape) {
        Column(modifier = Modifier.padding(start = 24.dp, top = 20.dp, end = 24.dp)) {
            Text(
                text = title,
                style = MaterialTheme.typography.caption.copy(color = Color.DarkGray)
            )
            Spacer(Modifier.height(8.dp))

            LazyColumn(
                modifier = Modifier
                    .weight(1f)
            ) {
                items(exploreList) { exploreItem ->

                    val index = exploreList.indexOf(exploreItem)

                    val offsetX = remember { mutableStateOf(0f) }
                    val offsetY = remember { mutableStateOf(0f) }

                    val width = remember { mutableStateOf(250f) }
                    val height = remember { mutableStateOf(1920f - 400 * index) }

                    Column(
                        Modifier
                            .fillParentMaxWidth()
                            .offset {
                                IntOffset(
                                    offsetX.value.roundToInt(),
                                    offsetY.value.roundToInt()
                                )
                            }
                            .pointerInput(Unit) {
                                forEachGesture {
                                    awaitPointerEventScope {
                                        val down = awaitFirstDown()
                                        var change =
                                            awaitTouchSlopOrCancellation(down.id) { change, over ->

                                                val original = Offset(offsetX.value, offsetY.value)
                                                val summed = original + over
                                                val newValue = Offset(
                                                    x = summed.x.coerceIn(
                                                        0f,
                                                        width.value - 10.dp.toPx()
                                                    ),
                                                    y = summed.y.coerceIn(
                                                        -400 * (index.toFloat()),
                                                        height.value - 150.dp.toPx()
                                                    )
                                                )
                                                change.consumePositionChange()
                                                offsetX.value = newValue.x
                                                offsetY.value = newValue.y
                                            }
                                        while (change != null && change.pressed) {
                                            change = awaitDragOrCancellation(change.id)
                                            if (change != null && change.pressed) {
                                                val original = Offset(offsetX.value, offsetY.value)
                                                val summed = original + change.positionChange()
                                                val newValue = Offset(
                                                    x = summed.x.coerceIn(
                                                        0f,
                                                        width.value - 10.dp.toPx()
                                                    ),
                                                    y = summed.y.coerceIn(
                                                        -400 * (index.toFloat()),
                                                        height.value - 150.dp.toPx()
                                                    )
                                                )
                                                change.consumePositionChange()
                                                offsetX.value = newValue.x
                                                offsetY.value = newValue.y
                                            }
                                        }
                                    }
                                }
                            }
                    ) {
                        ExploreItem(
                            list = exploreList,
                            modifier = Modifier.fillParentMaxWidth(),
                            item = exploreItem,
                            viewModel = viewModel,
                            onTagClicked = onTagClicked,
                            onRemoveClicked = onRemoveClicked
                        )
                        Divider(color = Color.LightGray)
                    }
                }
            }
        }
    }
}

@Composable
private fun ExploreItem(
    list: List<TodoModel>,
    item: TodoModel,
    viewModel: TodoViewModel,
    onTagClicked: (TodoModel) -> Unit,
    onRemoveClicked: (TodoModel) -> Unit,
    modifier: Modifier = Modifier,
) {
    var showConfirmationDialog by remember { mutableStateOf(false) }

    val backgroundColor: Color = if (viewModel.hashMap[item.name] == null) {
        try {
            viewModel.hashMap[list[list.indexOf(item) - 1].name] ?: Color.Transparent
        } catch (ex: Exception) {
            viewModel.hashMap[list[0].name] ?: Color.Transparent
        }
    } else {
        viewModel.hashMap[item.name]!!
    }

    Column(
        modifier = modifier
            .background(
                backgroundColor
            )
    ) {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .padding(12.dp)
        ) {
            ExploreImageContainer {
                CoilImage(
                    data = item.imageUrl,
                    fadeIn = true,
                    contentScale = ContentScale.Crop,
                    contentDescription = null,
                    loading = {
                        Box(Modifier.fillMaxSize()) {
                            Image(
                                modifier = Modifier
                                    .size(36.dp)
                                    .align(Alignment.Center),
                                painter = painterResource(id = R.drawable.screenshot),
                                contentDescription = null
                            )
                        }
                    }
                )
            }
            Spacer(Modifier.width(24.dp))
            Column {
                Text(
                    text = item.name,
                    style = MaterialTheme.typography.h6
                )
                Spacer(Modifier.height(8.dp))
                Text(
                    text = item.description,
                    style = MaterialTheme.typography.caption.copy(color = Color.DarkGray)
                )
            }
        }
        Row(
            modifier = modifier
                .fillMaxWidth()
                .padding(top = 12.dp, bottom = 12.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            IconButton(
                modifier = Modifier
                    .then(Modifier.size(48.dp)),
                onClick = {
                    onTagClicked(item)
                }
            ) {
                Icon(
                    Icons.Filled.Tag,
                    "Tag todo icon",
                    tint = Color.Green
                )
            }
            Spacer(Modifier.width(48.dp))
            IconButton(
                modifier = Modifier
                    .then(Modifier.size(48.dp)),
                onClick = {
                    showConfirmationDialog = true
                }
            ) {
                Icon(
                    Icons.Filled.Delete,
                    "Delete Todo icon button",
                    tint = Color.Red
                )
            }
        }
    }

    if (showConfirmationDialog) {
        Surface {
            AlertDialogComponent(
                item = item,
                onConfirmClicked = {
                    onRemoveClicked(it)
                },
                openDialog = showConfirmationDialog,
                showDialog = {
                    showConfirmationDialog = it
                }
            )
        }
    }
}

@Composable
private fun ExploreImageContainer(content: @Composable () -> Unit) {
    Surface(Modifier.size(width = 60.dp, height = 60.dp), RoundedCornerShape(4.dp)) {
        content()
    }
}

@Composable
fun AlertDialogComponent(
    item: TodoModel,
    onConfirmClicked: (TodoModel) -> Unit,
    openDialog: Boolean,
    showDialog: (Boolean) -> Unit
) {
    if (openDialog) {
        AlertDialog(
            onDismissRequest = {
                showDialog(false)
            },
            title = {
                Text(text = "Are you sure want to delete this Todo?", color = Color.White)
            },
            text = {
                Text("Use with cautious", color = Color.White)
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        onConfirmClicked(item)
                        showDialog(false)
                    }
                ) {
                    Text("YES", color = Color.White)
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        showDialog(false)
                    }
                ) {
                    Text("NO", color = Color.White)
                }
            },
            backgroundColor = colorResource(id = R.color.blue_800_light),
            contentColor = Color.White
        )
    }
}
