package com.internship.todolist

import androidx.compose.runtime.Immutable

@Immutable
data class TodoModel(
    val name: String,
    val description: String,
    val imageUrl: String
)
