package com.internship.todolist.add

import androidx.annotation.StringRes
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Button
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.google.accompanist.glide.GlideImage
import com.internship.todolist.R

@Composable
fun Question(
    question: Question,
    answer: Answer<*>?,
    onAnswer: (Answer<*>) -> Unit,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(start = 20.dp, end = 20.dp)
    ) {
        item {
            Spacer(modifier = Modifier.height(44.dp))
            val backgroundColor = if (MaterialTheme.colors.isLight) {
                MaterialTheme.colors.onSurface.copy(alpha = 0.04f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.06f)
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(
                        color = backgroundColor,
                        shape = MaterialTheme.shapes.small
                    )
            ) {
                Text(
                    text = stringResource(id = question.questionText),
                    style = MaterialTheme.typography.subtitle1,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 24.dp, horizontal = 16.dp)
                )
            }
            Spacer(modifier = Modifier.height(24.dp))
            if (question.description != null) {
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                    Text(
                        text = stringResource(id = question.description),
                        style = MaterialTheme.typography.caption,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 24.dp, start = 8.dp, end = 8.dp)
                    )
                }
            }
            when (question.answer) {

                is PossibleAnswer.Action -> ActionQuestion(
                    questionId = question.id,
                    possibleAnswer = question.answer,
                    answer = answer as Answer.Action?,
                    onAction = onAction,
                    modifier = Modifier.fillMaxWidth()
                )
                is PossibleAnswer.TitleInfo -> TitleQuestion(
                    possibleAnswer = question.answer,
                    answer = answer as Answer.TitleInfo?,
                    onAnswerSelected = { onAnswer(Answer.TitleInfo(it)) },
                    modifier = Modifier.fillMaxSize()
                )
                is PossibleAnswer.DescriptionInfo -> DescriptionQuestion(
                    possibleAnswer = question.answer,
                    answer = answer as Answer.DescriptionInfo?,
                    onAnswerSelected = { onAnswer(Answer.DescriptionInfo(it)) },
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    }
}

@Composable
private fun ActionQuestion(
    questionId: Int,
    possibleAnswer: PossibleAnswer.Action,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier = Modifier
) {
    when (possibleAnswer.actionType) {
        SurveyActionType.GET_PHOTO -> {
            PhotoQuestion(
                questionId = questionId,
                answerLabel = possibleAnswer.label,
                answer = answer,
                onAction = onAction,
                modifier = modifier
            )
        }
        SurveyActionType.TAKE_PHOTO -> {
            PhotoQuestion(
                questionId = questionId,
                answerLabel = possibleAnswer.label,
                answer = answer,
                onAction = onAction,
                modifier = modifier
            )
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun PhotoQuestion(
    questionId: Int,
    @StringRes answerLabel: Int,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        Button(
            onClick = { onAction(questionId, SurveyActionType.GET_PHOTO) },
            modifier = modifier.padding(vertical = 20.dp)
        ) {
            Text(text = stringResource(id = answerLabel))
        }

        Button(
            onClick = { onAction(questionId, SurveyActionType.TAKE_PHOTO) },
            modifier = modifier.padding(vertical = 20.dp)
        ) {
            Text(text = "TAKE PHOTO")
        }

        if (answer != null && answer.result is SurveyActionResult.Photo) {
            GlideImage(
                data = answer.result.uri!!,
                contentDescription = "Initiative photo",
                fadeIn = true,
                modifier = modifier.padding(20.dp)
            )
        } else {
            GlideImage(
                data = R.drawable.ic_baseline_image_not_supported_24,
                contentDescription = "Initiative photo default",
                fadeIn = true,
                modifier = modifier.padding(20.dp)
            )
        }
    }
}

@Composable
private fun TitleQuestion(
    possibleAnswer: PossibleAnswer.TitleInfo,
    answer: Answer.TitleInfo?,
    onAnswerSelected: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    var titleText by remember {
        mutableStateOf(answer?.title ?: possibleAnswer.defaultValue)
    }

    OutlinedTextField(
        value = titleText,
        onValueChange = {
            titleText = it
            onAnswerSelected(it)
        },
        label = { Text("title") },
        modifier = modifier
    )
}

@Composable
private fun DescriptionQuestion(
    possibleAnswer: PossibleAnswer.DescriptionInfo,
    answer: Answer.DescriptionInfo?,
    onAnswerSelected: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    var descriptionText by remember {
        mutableStateOf(answer?.description ?: possibleAnswer.defaultValue)
    }

    OutlinedTextField(
        value = descriptionText,
        onValueChange = {
            descriptionText = it
            onAnswerSelected(it)
        },
        label = { Text("description") },
        modifier = modifier
    )
}
