package com.internship.todolist

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.internship.todolist.add.SurveyActivity
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

private const val KEY_ARG_RESULT_NAME = "KEY_ARG_RESULT_NAME"
private const val KEY_ARG_RESULT_DESCRIPTION = "KEY_ARG_RESULT_DESCRIPTION"
private const val KEY_ARG_RESULT_URL = "KEY_ARG_RESULT_URL"

@AndroidEntryPoint
class LaunchActivity : ComponentActivity() {

    private val startForResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent: Intent? = result.data
            val name = intent?.getStringExtra(KEY_ARG_RESULT_NAME)
            val description = intent?.getStringExtra(KEY_ARG_RESULT_DESCRIPTION)
            val photoUrl = intent?.getStringExtra(KEY_ARG_RESULT_URL)
            when {
                name.isNullOrEmpty() -> {
                    throw IllegalStateException("KEY_ARG_RESULT_NAME arg cannot be null or empty")
                }
                description.isNullOrEmpty() -> {
                    throw IllegalStateException("KEY_ARG_RESULT_DESCRIPTION arg cannot be null or empty")
                }
                photoUrl.isNullOrEmpty() -> {
                    throw IllegalStateException("KEY_ARG_RESULT_URL arg cannot be null or empty")
                }
                else -> TodoViewModel.getInstance()!!.addTodo(
                    TodoModel(name = name, description = description, imageUrl = photoUrl)
                )
            }
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.plant(Timber.DebugTree())
        Timber.d("onCreate")

        requestPermissions()

        setContent {
            MainScreen(
                onAddItemClicked = {
                    startForResult.launch(Intent(this, SurveyActivity::class.java))
                }
            )
        }
    }

    private val permissionsCode = 101

    private fun requestPermissions() {
        val permissions = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        )
        ActivityCompat.requestPermissions(this, permissions, permissionsCode)
    }
}
