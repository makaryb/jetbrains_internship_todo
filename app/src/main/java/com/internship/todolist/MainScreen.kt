package com.internship.todolist

import androidx.annotation.VisibleForTesting
import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.internship.todolist.theme.TodolistTheme

@Composable
fun MyScaffold(content: @Composable () -> Unit) {
    TodolistTheme {
        Surface(color = MaterialTheme.colors.primary) {
            content()
        }
    }
}

@VisibleForTesting
@Composable
fun MainScreen(
    onAddItemClicked: () -> Unit
) {
    MyScaffold {
        val transitionState = remember {
            MutableTransitionState(SplashState.Shown)
        }

        val transition = updateTransition(transitionState, label = "")

        val splashAlpha by transition.animateFloat(
            transitionSpec = { tween(durationMillis = 100) }, label = ""
        ) {
            if (it == SplashState.Shown) 1f else 0f
        }

        val contentAlpha by transition.animateFloat(
            transitionSpec = { tween(durationMillis = 300) }, label = ""
        ) {
            if (it == SplashState.Shown) 0f else 1f
        }

        val contentTopPadding by transition.animateDp(
            transitionSpec = { spring(stiffness = Spring.StiffnessLow) }, label = ""
        ) {
            if (it == SplashState.Shown) 100.dp else 0.dp
        }

        Box {
            LandingScreen(
                modifier = Modifier.alpha(splashAlpha),
                onTimeout = { transitionState.targetState = SplashState.Completed }
            )
            MainContent(
                modifier = Modifier.alpha(contentAlpha),
                topPadding = contentTopPadding,
                onAddItemClicked = onAddItemClicked
            )
        }
    }
}

@Composable
private fun MainContent(
    modifier: Modifier = Modifier,
    topPadding: Dp = 0.dp,
    onAddItemClicked: () -> Unit
) {
    Column(modifier = modifier) {
        Spacer(Modifier.padding(top = topPadding))
        TodoScreen(
            onAddItemClicked = onAddItemClicked
        )
    }
}

enum class SplashState { Shown, Completed }
