package com.internship.todolist.buildsrc

object Versions {
    const val ktlint = "0.40.0"
    const val ktlintReporter = "0.2.1"

    const val junitVersion = "4.13.2"
}

object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:7.0.0-alpha14"
    const val jdkDesugar = "com.android.tools:desugar_jdk_libs:1.0.9"

    const val junit = "junit:junit:4.13"

    const val material = "com.google.android.material:material:1.1.0"

    object Accompanist {
        private const val version = "0.6.0"
        const val coil = "dev.chrisbanes.accompanist:accompanist-coil:$version"
    }

    object Kotlin {
        private const val version = "1.4.30"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
        const val extensions = "org.jetbrains.kotlin:kotlin-android-extensions:$version"
    }

    object Coroutines {
        private const val version = "1.4.1"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
        const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
    }

    object AndroidX {

        const val appcompat = "androidx.appcompat:appcompat:1.2.0-rc01"
        const val coreKtx = "androidx.core:core-ktx:1.5.0-alpha01"

        object Lifecycle {
            private const val version = "2.3.0-beta01"
            const val viewModelCompose = "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha02"
            const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
            const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:$version"
            const val commonJava8 = "androidx.lifecycle:lifecycle-common-java8:$version"
            const val viewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.0"
        }

        object Activity {
            private const val version = "1.2.0"
            const val activityKtx = "androidx.activity:activity-ktx:$version"
            const val activityCompose = "androidx.activity:activity-compose:1.3.0-alpha03"
        }

        object AppCompat {
            private const val version = "1.2.0"
            const val appcompat = "androidx.appcompat:appcompat:$version"
        }

        object Compose {
            const val snapshot = ""
            const val version = "1.0.0-beta01"

            @get:JvmStatic
            val snapshotUrl: String
                get() = "https://androidx.dev/snapshots/builds/$snapshot/artifacts/repository/"

            const val ui = "androidx.compose.ui:ui:$version"
            const val viewBinding = "androidx.compose.ui:ui-viewbinding:1.0.0-alpha12"
            const val foundation = "androidx.compose.foundation:foundation:$version"
            const val layout = "androidx.compose.foundation:foundation-layout:$version"
            const val material = "androidx.compose.material:material:$version"
            const val materialIconsCore = "androidx.compose.material:material-icons-core:$version"
            const val materialIconsExtended = "androidx.compose.material:material-icons-extended:$version"
            const val runtime = "androidx.compose.runtime:runtime:$version"
            const val runtimeLivedata = "androidx.compose.runtime:runtime-livedata:$version"
            const val tooling = "androidx.compose.ui:ui-tooling:$version"
            const val liveData = "androidx.compose.runtime:runtime-livedata:$version"
            const val rxJava2 = "androidx.compose.runtime:runtime-rxjava2:$version"
            const val uiTest = "androidx.compose.ui:ui-test:$version"
            const val uiTestJunit4 = "androidx.compose.ui:ui-test-junit4:$version"
        }

        object Navigation {
            private const val version = "2.3.3"
            const val fragment = "androidx.navigation:navigation-fragment-ktx:$version"
            const val uiKtx = "androidx.navigation:navigation-ui-ktx:$version"

            const val compose = "androidx.navigation:navigation-compose:1.0.0-alpha08"
        }

        object UI {
            private const val materialVersion = "1.3.0"
            private const val constraintLayoutVersion = "2.0.4"
            private const val recyclerViewVersion = "1.1.0"
            const val material = "com.google.android.material:material:$materialVersion"
            const val constraintLayout = "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"
            const val recyclerView = "androidx.recyclerview:recyclerview:$recyclerViewVersion"
        }

        object Test {
            private const val version = "1.2.0"
            const val core = "androidx.test:core:$version"
            const val rules = "androidx.test:rules:$version"

            object Ext {
                private const val version = "1.1.2-rc01"
                const val junit = "androidx.test.ext:junit-ktx:$version"
            }

            const val espressoCore = "androidx.test.espresso:espresso-core:3.3.0"
        }
    }

    object Android {

        object DataBinding {
            private const val version = "3.2.0-alpha10"
            const val compiler = "com.android.databinding:compiler:$version"
        }
    }

    object Google {

        object AccompanistGlide {
            private const val version = "0.7.0"
            const val glide = "com.google.accompanist:accompanist-glide:$version"
        }
    }

    object Hilt {
        private const val version = "2.31.2-alpha"

        const val gradlePlugin = "com.google.dagger:hilt-android-gradle-plugin:$version"
        const val android = "com.google.dagger:hilt-android:$version"
        const val compiler = "com.google.dagger:hilt-compiler:$version"
        const val testing = "com.google.dagger:hilt-android-testing:$version"
    }

    object Logging {
        private const val version = "4.7.1"
        const val timber = "com.jakewharton.timber:timber:$version"
    }
}
