package com.internship.todolist.theme

import androidx.compose.ui.graphics.Color

val Purple300 = Color(0xFFCD52FC)
val Purple600 = Color(0xFF9F00F4)
val Purple700 = Color(0xFF8100EF)
val Blue800 = Color(0xFF212A50)
val Blue800Light = Color(0xEF212A50)

val Green = Color(0xFF64DD17)

val Red300 = Color(0xFFD00036)
val Red800 = Color(0xFFEA6D7E)

val Gray100 = Color(0xFFF5F5F5)
val Gray900 = Color(0xFF212121)

val LowGray900 = Color(0x33212121)
val White = Color(0xFFFFFFFF)

val LowGreen = Color(0x7764DD17)
val LowRed = Color(0x77D00036)

val FABColor = Color(0xFFEFEFEF)
val FABApproveColor = Color(0xFF64DD17)
