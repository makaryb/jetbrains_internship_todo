package com.internship.todolist

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodoLocalDatasource @Inject constructor() {

    val todolist: List<TodoModel> = listOf(
        TodoModel(
            name = "JetBrains Internship",
            description = "https://internship.jetbrains.com/",
            imageUrl = "https://nodesk.co/remote-companies/assets/logos/jetbrains.ea4da2c79f1995167b687b1111a171c982017a3f788f8d0e873081f3e63d2374.jpg"
        ),
        TodoModel(
            name = "EPAM Internship",
            description = "https://www.training.epam.com/",
            imageUrl = "https://res-3.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_170,w_170,f_auto,b_white,q_auto:eco/hs0whppqg2pi33o6q8op"
        ),
        TodoModel(
            name = "Mail ru Internship",
            description = "https://corp.mail.ru/internship/",
            imageUrl = "https://blog.mail.ru/wp-content/uploads/2019/02/Logo_mail.png"
        ),
        TodoModel(
            name = "OK tech Internship",
            description = "https://oktech.ru/pages/internship/",
            imageUrl = "https://ucare.timepad.ru/eea91d72-ee01-4e32-90f9-da6a0052c275/-/preview/308x600/-/format/jpeg/logo_org_155364.jpg"
        ),
    )
}
